function compress_video
    set delete false
    if test "$argv[1]" = "-d"
        set delete true
        set infile "$argv[2]"
    else
        set infile "$argv[1]"
    end

    if test -z "$infile"
        echo "missing file"
        return
    else
        ffmpeg -i "$infile" -map_metadata 0 -vcodec hevc_nvenc -acodec copy "$infile".mkv
        touch -d "@"(stat -c %Y "$infile") "$infile".mkv

        if $delete
            rm -f "$infile"
        end

        set newname (echo "$infile" | sed -e 's/\.[^.]*$/.mkv/')
        mv "$infile".mkv "$newname"
    end
end
