#!/usr/bin/fish

function __fish_zl_layout_valid
  if head -n 1 $argv[1] | grep -q '^[ ]*layout[ ]*{'
    basename "$argv[1]"
  end
end

function __fish_zl_list_layouts
  if test "$argv[1]" = "global"
    set layout_dir (zellij setup --dump-config | grep -v '^[\s]*//' | grep layout_dir)
    if test -z "$layout_dir"
      set layout_dir "$XDG_CONFIG_HOME/zellij/layouts"
    end
    if test -d "$layout_dir"
      for file in (find "$layout_dir" -maxdepth 1 -type f)
        __fish_zl_layout_valid "$file" | sed -e 's/\.kdl$//'
      end
    end
  else
    for file in (find -maxdepth 1 -type f)
      __fish_zl_layout_valid "$file"
    end
  end
end

function __fish_zl_layout_selected
  set tokens (commandline -cpo)
  if test (count $tokens) -lt 2
    return 1
  end
  contains "$tokens[2]" (__fish_zl_list_layouts global) (__fish_zl_list_layouts local)
end

complete -c zl -f
complete -c zl -n "not __fish_zl_layout_selected" -d "global layout" -a '(__fish_zl_list_layouts global)'
complete -c zl -n "not __fish_zl_layout_selected" -d "local layout" -a '(__fish_zl_list_layouts local)'
complete -c zl -n "__fish_zl_layout_selected" -s "s" -l "session" -d "Specify session name" -r -f

function zl -d "run predefined zellij layout"
  set unlock false
  if set -q ZELLIJ_SESSION_NAME
    set unlock true
    zellij ac switch-mode locked
  end
  zellij -l $argv
  if $unlock
    zellij ac switch-mode normal
  end
end

