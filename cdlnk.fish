#!/usr/bin/fish

function cdlnk -d 'change directory following windows .lnk files'
    if test -z "$argv[1]"
        echo "Missing link argument"
        return 255
    end

    if test ! -f "$argv[1]"
        echo  "'$argv[1]' is not a readable link file"
        return 255
    end

    set link ( lnkinfo "$argv[1]" | grep Network | cut -d: -f2 | sed -e "s|\\\|\/|g" -e 's/^[[:space:]]*//' )
    
    for mp in (mount)
        set remote ( echo "$mp" | sed -e 's/ on .*//' )
        if string match "$remote"'*' "$link" > /dev/null
            set localmount ( echo "$mp" | sed -e 's/.* on \(.*\) type .*/\1/')
            set target ( string replace "$remote" "$localmount" "$link")
        end
    end

    if test -z "$target"
        echo "cannot resolve link: $link"
        return 255
    end

    cd "$target"
end
