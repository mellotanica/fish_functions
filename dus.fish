function dus
	if test (count $argv) -gt 0;
		du -sh $argv | sort -h;
	else;
		du -sh (/usr/bin/ls -A) | sort -h;
	end
end
