function __fish_is_dir_root
    for marker in .git .svn .hg .brz Makefile CMakeLists.txt .scuba.yml .scuba.yaml .deps.udm
        if test -e "$argv[1]/$marker"
            return 0
        end
    end
    return 1
end

function __fish_find_project_root
    if __fish_is_dir_root "$argv[1]"
        echo "$argv[1]"
        return 0
    else if test "$argv[1]" != "$(dirname "$argv[1]")"
        __fish_find_project_root "$(dirname "$argv[1]")"
        return $status
    else
        return 1
    end
end

function cdr -d "Change directory to the root of the current project"
    set proot "$(__fish_find_project_root "$PWD")"
    if test $status -eq 0
        cd "$proot"
    else
        echo "No project directory found"
    end
end
