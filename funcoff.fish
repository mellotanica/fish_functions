function funcoff-
	if test (count $argv) -ge 1
		if test "$argv[1]" = "-h"
			if test (count $argv) -gt 1
				echo "real_poweroff $argv[2..-1]"
				return 0
			else
				echo "real_poweroff"
				return 0
			end
		end
	end
	ps -A | egrep '(Xorg|startkde)' > /dev/null
	if test $status = 0
		echo "kde on, poweroff gently"
	else
		echo "real_poweroff $argv"
	end
end
