#!/usr/bin/fish

set passcomp (pkg-config --variable completionsdir fish)/pass.fish
if test -f "$passcomp"
    function __fish_sp_pass_ok
        sp --checkpass (commandline -cpo) >/dev/null 2>&1
        echo $status
    end

    function __fish_sp_host_ok
        sp --checkhost (commandline -cpo) >/dev/null 2>&1
        echo $status
    end

    function __fish_sp_completing_user
        set lasttoken (commandline -cpo)[-1]
        set value (string match -- "$lasttoken" "-u"; or string match -- "$lasttoken" "--user")
        echo $value
        return (test -n "$value")
    end

    function __fish_sp_completing_prefix
        set lasttoken (commandline -cpo)[-1]
        set value (string match -- "$lasttoken" "-p"; or string match -- "$lasttoken" "--prefix")
        echo $value
        return (test -n "$value")
    end

    source "$passcomp"
    complete -c sp -f
    complete -c sp -w sshpass
    complete -c sp -n 'test (__fish_sp_host_ok) -ne 0 -a -z (__fish_sp_completing_prefix)' -d "Host" -a '(sp --completehost (commandline -cpo))'
    complete -c sp -n '__fish_sp_host_ok' -s u -l user -d "Select host user" -a '(sp --completepass (commandline -cpo))'
    complete -c sp -n '__fish_sp_completing_user' -d "User" -a '(sp --completepass (commandline -cpo))'
    complete -c sp -n '__fish_sp_completing_prefix' -d "Prefix" -a '(sp --completeprefix)'
    complete -c sp -n 'test (__fish_number_of_cmd_args_wo_opts) -ge 2 -a (__fish_sp_pass_ok) -eq 0 -a -z (__fish_sp_completing_user) -a -z (__fish_sp_completing_prefix)' -d 'Command to run' -a '(__fish_complete_subcommand --fcs-skip=2 -u --user -p --prefix)'
end

function sp -d 'pass-backed sshpass wrapper'
    if set -q SP_GLOBAL_PREFIX
        set prefix "$SP_GLOBAL_PREFIX"
    else
        set prefix ssh
    end

    if set -q SP_GLOBAL_USER
        set dfluser "$SP_GLOBAL_USER"
    else
        set dfluser (whoami)
    end

    argparse -i 'completeprefix' -- $argv

    if set -q _flag_completeprefix
        __fish_pass_print_entry_dirs
        return
    end

    argparse -i 'h/help' 'p/prefix=' -- $argv

    if set -q _flag_prefix
        if test -n "$_flag_prefix"
            set prefix "$_flag_prefix"
        end
    end

    set prefix (string trim -r -c '/' -- "$prefix")

    if set -q _flag_help
        echo "usage: "(status function)" [-h] host [-u USER] COMMAND ARGS..."
        echo
        echo "run COMMAND (typically ssh or scp) with sshpass using host and USER to get"
        echo "the password from local pass db (prefix: $prefix)"
        echo "e.g."
        echo "  ~> sp myserver ssh serverhost"
        echo "    ==> sshpass -p(pass show $prefix/myserver/$dfluser | head -n 1) ssh serverhost"
        echo
        echo "prefix can be set by exporting global variable SP_GLOBAL_PREFIX"
        echo "default user '$dfluser' can be set by exporting global variable SP_GLOBAL_USER"
        return
    end

    argparse -i 'completehost' 'completepass' 'checkhost' 'checkpass' -- $argv

    if set -q _flag_completehost
        __fish_pass_print_entry_dirs | grep "$prefix" | sed -e "s_$prefix/\?__" -e 's_/$__'
        return
    end

    set cmdstart 1
    if set -q _flag_completepass
        set cmdstart 2
    end
    if set -q _flag_checkhost
        set cmdstart 2
    end
    if set -q _flag_checkpass
        set cmdstart 2
    end

    set pd (__fish_pass_get_prefix)

    set host "$argv[$cmdstart]"
    if test -z "$host"
        echo "missing host name"
        return 255
    end
    set passquery "$prefix/$host"
    if not test -d "$pd/$passquery"
        echo "unknown host: $host ($pd/$passquery)"
        return 255
    end
    if set -q _flag_checkhost
        return 0
    end

    set cmdstart (math $cmdstart + 1)

    if set -q _flag_completepass
        __fish_pass_print_entries | grep "$passquery" | sed -e "s_$passquery/\?__"
        return
    end

    argparse -i 'u/user=' -- $argv

    if set -q _flag_user
        set user "$_flag_user"
    else
        set user "$dfluser"
    end

    set passquery "$passquery/$user"

    if not test -f "$pd/$passquery.gpg"
        echo "unknown user '$user' for host '$host' ($pd/$passquery.gpg)"
        return 255
    end
    if set -q _flag_checkpass
        return 0
    end

    command sshpass -p(pass show "$passquery" | head -n 1) $argv[$cmdstart..-1]
end

