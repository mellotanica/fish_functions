#!/usr/bin/fish

function zellij-clean-sessions -d "clean dead sessions and active ones named 'del*'"
    for session in (zellij list-sessions -ns | grep -e '^del[-0-9]\+.*');
        zellij kill-session "$session"
    end; zellij delete-all-sessions -y
end
