
function bela
	if not set -q BELA_ENV
		set -g BELA_ENV bela
		set -g VIRTUAL_ENV bela
		set -g BELA_P_PATH $PATH
		set -x PATH ~/repos/Bela/scripts $PATH
		set -g BELA_P_DIR $PWD
		cd ~/repos/bela_projects
	end
end

function bela_exit
	if set -q BELA_ENV
		set -e BELA_ENV
		set -e VIRTUAL_ENV
		set -x PATH $BELA_P_PATH
		set -e BELA_P_PATH
		cd "$BELA_P_DIR"
		set -e BELA_P_DIR
	end
end
